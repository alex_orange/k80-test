#!/usr/bin/python

"""
Simple get one node by id. Used for testing GPUs.

Instructions:

Run /local/repository/test.sh to test.
Run nvidia-smi --loop=1 in another terminal to watch the temperature/GPU usage.
"""

import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab.pnext as pn
import geni.rspec.igext as ig
import geni.rspec.emulab.spectrum as spectrum


portal.context.defineParameter("node_id",
                               "ID of the node to get",
                               portal.ParameterType.STRING, "")

params = portal.context.bindParameters()
request = portal.context.makeRequestRSpec()

node = request.RawPC("node")
node.component_id = params.node_id
node.disk_image = "urn:publicid:IDN+emulab.net+image+PowderTeam:gpu-test"


portal.context.printRequestRSpec()
