#!/bin/bash -e

export DISTRO=ubuntu2004
export ARCHITECTURE=x86_64
sudo apt update
sudo add-apt-repository -y ppa:graphics-drivers
sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/${DISTRO}/${ARCHITECTURE}/7fa2af80.pub
sudo bash -c "echo deb\ http://developer.download.nvidia.com/compute/cuda/repos/${DISTRO}/${ARCHITECTURE}/\ / > /etc/apt/sources.list.d/cuda.list"
sudo bash -c "echo deb\ http://developer.download.nvidia.com/compute/machine-learning/repos/${DISTRO}/${ARCHITECTURE}\ / > /etc/apt/sources.list.d/cuda_learn.list"

sudo apt-get update
sudo apt-get install -y nvidia-headless-465 nvidia-utils-465
#sudo apt-get install -y nvidia-driver-465 || /bin/true
sudo modprobe nvidia
sleep 5
nvidia-smi

sudo apt-get install -y --no-install-recommends cuda-11-0
sudo apt-get install libcudnn8

sudo add-apt-repository multiverse
sudo apt-get install -y python3-virtualenv

#sudo apt-get install -y libcudart10.1 libcublas10 libcublaslt10 libcufft10 libcurand10 libcusolver10 libcusparse10
#tar xf cudnn-10.1-linux-x64-v7.6.5.32.tgz 
#sudo mv cuda /opt

sudo apt-get install -y libcublas-11-1 libcufft-11-0 libcurand-11-0 libcusolver-11-0 libcusparse-11-0


sudo mount -t tmpfs -o size=16g tmpfs /tmp
cd /tmp

virtualenv -p python3 ve
ve/bin/pip install tensorflow-gpu==2.4.0 matplotlib

#export LD_LIBRARY_PATH=/opt/cuda/lib64:$LD_LIBRARY_PATH

################################################################################
# While this runs, run nvidia-smi -l --loop=1 in another terminal to watch the
# temps.
################################################################################

ve/bin/python <<END
import tensorflow as tf

device_name = tf.test.gpu_device_name()

# should print "Found GPU at: /device:GPU:0"
print('Found GPU at: {}'.format(device_name))

import numpy as np
import matplotlib.pyplot as plt

print(tf.__version__)

fashion_mnist = tf.keras.datasets.fashion_mnist

(train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()

class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat',
               'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']

train_images = train_images / 255.0

test_images = test_images / 255.0

with tf.device('/GPU:3'):
    model = tf.keras.Sequential([
        tf.keras.layers.Flatten(input_shape=(28, 28)),
        tf.keras.layers.Dense(16384, activation='relu'),
        tf.keras.layers.Dense(2048, activation='relu'),
        tf.keras.layers.Dense(2048, activation='relu'),
        tf.keras.layers.Dense(10)
    ])

    model.compile(optimizer='adam',
                  loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                  metrics=['accuracy'])

    model.fit(train_images, train_labels, epochs=1000)
END
