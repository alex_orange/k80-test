#!/bin/bash

sudo modprobe nvidia

sudo mount -t tmpfs -o size=16g tmpfs /tmp
cd /tmp

virtualenv -p python3 ve
ve/bin/pip install tensorflow-gpu==2.4.0 matplotlib

################################################################################
# While this runs, run nvidia-smi -l --loop=1 in another terminal to watch the
# temps.
################################################################################

ve/bin/python <<END
import tensorflow as tf

device_name = tf.test.gpu_device_name()

# should print "Found GPU at: /device:GPU:0"
print('Found GPU at: {}'.format(device_name))

import numpy as np
import matplotlib.pyplot as plt

print(tf.__version__)

fashion_mnist = tf.keras.datasets.fashion_mnist

(train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()

class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat',
               'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']

train_images = train_images / 255.0

test_images = test_images / 255.0

with tf.device('/GPU:0'):
    model = tf.keras.Sequential([
        tf.keras.layers.Flatten(input_shape=(28, 28)),
        tf.keras.layers.Dense(16384, activation='relu'),
        tf.keras.layers.Dense(2048, activation='relu'),
        tf.keras.layers.Dense(2048, activation='relu'),
        tf.keras.layers.Dense(10)
    ])

    model.compile(optimizer='adam',
                  loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                  metrics=['accuracy'])

    model.fit(train_images, train_labels, epochs=1000)
END

